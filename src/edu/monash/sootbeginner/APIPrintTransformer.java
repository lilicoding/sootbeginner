package edu.monash.sootbeginner;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import soot.Body;
import soot.PatchingChain;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.Stmt;
import soot.util.Chain;

public class APIPrintTransformer extends SceneTransformer
{

	/**
	 * Print Android APIs (starting with com.android)
	 */
	@Override
	protected void internalTransform(String phaseName, Map<String, String> options) 
	{
		//(1) Obtain all application classes
		Chain<SootClass> sootClasses = Scene.v().getApplicationClasses();
		
		for (Iterator<SootClass> iter = sootClasses.snapshotIterator(); iter.hasNext();)
		{
			SootClass sc = iter.next();
			
			//(2) Obtain all the methods from a given class
			List<SootMethod> sootMethods = sc.getMethods();
			
			for (int i = 0; i < sootMethods.size(); i++)
			{
				SootMethod sm = sootMethods.get(i);
				
				try
				{
					Body body = sm.retrieveActiveBody();
					
					//(3) Obtain all statements from a given method
					PatchingChain<Unit> units = body.getUnits();
					
					for (Iterator<Unit> unitIter = units.snapshotIterator(); unitIter.hasNext(); )
					{
						Stmt stmt = (Stmt) unitIter.next();
						
						//(4) Check if the statement is related to method invocation 
						if (stmt.containsInvokeExpr())
						{
							SootMethod callee = stmt.getInvokeExpr().getMethod();
							SootClass calleeClass = callee.getDeclaringClass();
							String calleeClassName = calleeClass.getName();
							
							if (calleeClassName.startsWith("android."))
							{
								System.out.println(callee.getSignature() + " is called by " + sm.getSignature());
							}
						}
					}
				}
				catch (Exception ex)
				{
					//TODO: No active body retrieved from the method 
				}
				
			}
		}
	}
}
